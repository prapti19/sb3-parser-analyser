package sb3.modifiedParser

case class ProgramEl(stage: SpriteEl, sprites: List[SpriteEl])
case class SpriteEl(scripts: List[ScriptEl], vars: List[VariableEl] = List(), name: String, lists: List[VariableEl])
case class ScriptEl(topBlock: BlockEl, topBlockID: String, scriptseq: String)
case class BlockEl(opcode: String, id: String, next: BlockEl, parent: BlockEl, inputs: List[InputEl], fields: List[FieldEl])
case class VariableEl(id: String, name: String)
case class InputEl(iptype: String)
case class FieldEl(fieldtype: String, value: String, variable: VariableEl)
