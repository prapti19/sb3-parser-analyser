package sb3.modifiedParser
import play.api.libs.json.JsArray
import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsValue
import play.api.libs.json.JsNull
import play.api.libs.json.Json
import play.api.libs.json.Reads.BooleanReads
import play.api.libs.json.Reads.JsArrayReads
import play.api.libs.json.Reads.JsObjectReads
import play.api.libs.json.Reads.JsStringReads
import play.api.libs.json.Reads.StringReads
import sb3.modifiedParser._
import sb3.analyzer.LongScriptAnalysis
import sb3.analyzer.UncommunicativeNameAnalysis
import sb3.analyzer.DuplicateCodeAnalysis
import sb3.analyzer.DuplicateExpressionAnalysis

class Sb3Json2AST {
  def programEl(json: JsValue): ProgramEl = {
    val targets = (json \ "targets").get.as[JsArray]
    val stage = targets.value.filter { jsVal => (jsVal \ "isStage").as[Boolean] == true }.map(tar => spriteEl(tar.as[JsObject])).apply(0)
    val sprites = targets.value.filter { jsVal => (jsVal \ "isStage").as[Boolean] == false }.map(tar => spriteEl(tar.as[JsObject]))
    ProgramEl(stage, sprites.toList)
  }

  def spriteEl(jsObj: JsObject): SpriteEl = {
    val scriptList: List[ScriptEl] = scriptListEl((jsObj \ "blocks").as[JsObject])
    val variableList: List[VariableEl] = variableListEl((jsObj \ "variables").as[JsObject])
    val spriteName: String = (jsObj \ "name").as[String]
    val lists: List[VariableEl] = variableListEl((jsObj \ "lists").as[JsObject])
    SpriteEl(vars = variableList, scripts = scriptList, name = spriteName, lists = lists)
  }

  def scriptListEl(blockMap: JsObject): List[ScriptEl] = {
    var scripts = List.empty[ScriptEl]
    val topLevels = blockMap.value.filter { case (id, jsObj) => (jsObj \ "topLevel").as[Boolean] }
    if (!topLevels.isEmpty) {
      for ((topLevBlockId, topLevJsObj) <- topLevels) {
        val script = scriptEl(topLevBlockId, blockMap)
        scripts = script +: scripts
      }
    }
    scripts

  }

  def variableListEl(blockMap: JsObject): List[VariableEl] = {
    var variables = List.empty[VariableEl]
    for ((varId, varArr) <- blockMap.value) {
      val variable = VariableEl(varId, varArr.apply(0).as[String])
      variables = variable +: variables
    }
    variables
  }

  def inputListEl(blockMap: JsObject): List[InputEl] = {
    var inputs = List.empty[InputEl]
    for ((inputType, varArr) <- blockMap.value) {
      val input = InputEl(inputType)
      inputs = input +: inputs
    }
    inputs
  }

  def fieldListEl(blockMap: JsObject): List[FieldEl] = {
    var fields = List.empty[FieldEl]
    for ((fieldType, varArr) <- blockMap.value) {
      var input: FieldEl = null
      if (fieldType.equals("VARIABLE")) {
        val id = varArr.apply(1).as[String]
        val name = varArr.apply(0).as[String]
        input = FieldEl(fieldtype = fieldType, value = "", variable = VariableEl(id, name))
      } else {
        input = FieldEl(fieldType, varArr.apply(0).as[String], null)
      }
      fields = input +: fields
    }
    fields
  }

  def scriptEl(topBlockId: String, blockMap: JsObject): ScriptEl = {
    var script: ScriptEl = new ScriptEl(null, topBlockId, null)
    val topblock = blockEl(blockMap, topBlockId, null)
    script = script.copy(topBlock = topblock)
    script
  }

  def blockEl(blockJsObj: JsObject, id: String, parent: BlockEl): BlockEl = {
    val opcode = (blockJsObj \ id \ "opcode").as[String]
    val nextRes = (blockJsObj \ id \ "next")
    val inputs: List[InputEl] = inputListEl((blockJsObj \ id \ "inputs").as[JsObject])
    val fields: List[FieldEl] = fieldListEl((blockJsObj \ id \ "fields").as[JsObject])
    var currBlock: BlockEl = BlockEl(opcode, id, null, parent, inputs, fields)
    val next: BlockEl = if (nextRes.get != JsNull) {
      blockEl(blockJsObj, nextRes.as[String], currBlock)
    } else null
    currBlock = currBlock.copy(next = next)
    currBlock
  }

  def analyze(jsonstr: String): String = {

    val jsVal = Json.parse(jsonstr)
    val res = new Sb3Json2AST().programEl(jsVal)
    println(res)
    println("=========================================================================================")

    var resJson: JsObject = JsObject(Seq.empty)

    resJson = resJson + ("LongScript" -> new LongScriptAnalysis().analyze(res))
    resJson = resJson + ("UncommunicativeName" -> new UncommunicativeNameAnalysis().analyze(res))
    resJson = resJson + ("DuplicateCode" -> new DuplicateCodeAnalysis().analyze(jsonstr))
    resJson = resJson + ("DuplicateExpression" -> new DuplicateExpressionAnalysis().analyze(jsonstr))
//    resJson = resJson + ("BroadVariableScope" -> new BroadVariableScopeAnalysis().analyze(res))

    resJson.toString()
  }

}

object Sb3Json2AST extends App {
//  val jsonstr = """
//{"targets":[{"isStage":true,"name":"Stage","variables":{"`jEk@4|i[#Fk?(8x)AV.-my variable":["my variable",0],"Y})TGj3O`[./JoG:jwvt":["1234",0]},"lists":{},"broadcasts":{},"blocks":{},"currentCostume":0,"costumes":[{"assetId":"cd21514d0531fdffb22204e0ec5ed84a","name":"backdrop1","md5ext":"cd21514d0531fdffb22204e0ec5ed84a.svg","dataFormat":"svg","rotationCenterX":1,"rotationCenterY":1}],"sounds":[{"assetId":"83a9787d4cb6f3b7632b4ddfebf74367","name":"pop","dataFormat":"wav","format":"","rate":44100,"sampleCount":1032,"md5ext":"83a9787d4cb6f3b7632b4ddfebf74367.wav"}],"volume":100,"tempo":60,"videoTransparency":50,"videoState":"off"},{"isStage":false,"name":"Sprite1","variables":{},"lists":{},"broadcasts":{},"blocks":{"@/O)M4y8_($x~d]L}$U6":{"opcode":"motion_movesteps","next":"M?{T{,6@P8:wyMb;`X*M","parent":null,"inputs":{"STEPS":[3,"4qq%ah84}j5|}qj1ng+p",[4,"10"]]},"fields":{},"topLevel":true,"shadow":false,"x":412,"y":191},"4qq%ah84}j5|}qj1ng+p":{"opcode":"operator_random","next":null,"parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"FROM":[1,[4,"1"]],"TO":[1,[4,"10"]]},"fields":{},"topLevel":false,"shadow":false},"M?{T{,6@P8:wyMb;`X*M":{"opcode":"motion_turnright","next":null,"parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"DEGREES":[1,[4,"15"]]},"fields":{},"topLevel":false,"shadow":false}},"currentCostume":0,"costumes":[{"assetId":"09dc888b0b7df19f70d81588ae73420e","name":"costume1","bitmapResolution":1,"md5ext":"09dc888b0b7df19f70d81588ae73420e.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55},{"assetId":"3696356a03a8d938318876a593572843","name":"costume2","bitmapResolution":1,"md5ext":"3696356a03a8d938318876a593572843.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55}],"sounds":[{"assetId":"83c36d806dc92327b9e7049a565c6bff","name":"Meow","dataFormat":"wav","format":"","rate":44100,"sampleCount":37376,"md5ext":"83c36d806dc92327b9e7049a565c6bff.wav"}],"volume":100,"visible":true,"x":0,"y":0,"size":100,"direction":90,"draggable":false,"rotationStyle":"all around"},{"isStage":false,"name":"Trees","variables":{},"lists":{},"broadcasts":{},"blocks":{"r/SAl@~c.KJhpmkZ`pDF":{"opcode":"data_setvariableto","next":"G1^19a55Lvjy#yrA5j[d","parent":null,"inputs":{"VALUE":[1,[10,"0"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":true,"shadow":false,"x":243,"y":96},"G1^19a55Lvjy#yrA5j[d":{"opcode":"data_changevariableby","next":"!2cvzsu;@JhqPwzThlO|","parent":"r/SAl@~c.KJhpmkZ`pDF","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"!2cvzsu;@JhqPwzThlO|":{"opcode":"data_changevariableby","next":"V+)c,1?u#R;{ojsq*j)M","parent":"G1^19a55Lvjy#yrA5j[d","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"V+)c,1?u#R;{ojsq*j)M":{"opcode":"data_changevariableby","next":"728wFTnx}oJf~%1!x=rx","parent":"!2cvzsu;@JhqPwzThlO|","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"728wFTnx}oJf~%1!x=rx":{"opcode":"data_changevariableby","next":"d_ZqRTo1|pWKa]XlvorA","parent":"V+)c,1?u#R;{ojsq*j)M","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"d_ZqRTo1|pWKa]XlvorA":{"opcode":"data_changevariableby","next":"mUn1UDqpsalM_-/;H)w/","parent":"728wFTnx}oJf~%1!x=rx","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"mUn1UDqpsalM_-/;H)w/":{"opcode":"data_changevariableby","next":"zSuuoa+|e4OZ.kG0AH,%","parent":"d_ZqRTo1|pWKa]XlvorA","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"zSuuoa+|e4OZ.kG0AH,%":{"opcode":"data_changevariableby","next":"6VGdPXpqZ8B[LFlH`+2?","parent":"mUn1UDqpsalM_-/;H)w/","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"6VGdPXpqZ8B[LFlH`+2?":{"opcode":"data_changevariableby","next":"_`xsixbi%[/x[yQpwGLp","parent":"zSuuoa+|e4OZ.kG0AH,%","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"k0|]3OwyixT#+_mgNc)g":{"opcode":"data_changevariableby","next":"4!aQqR|*RjV,E1`bMu)6","parent":"_`xsixbi%[/x[yQpwGLp","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"_`xsixbi%[/x[yQpwGLp":{"opcode":"data_changevariableby","next":"k0|]3OwyixT#+_mgNc)g","parent":"6VGdPXpqZ8B[LFlH`+2?","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"4!aQqR|*RjV,E1`bMu)6":{"opcode":"data_changevariableby","next":null,"parent":"k0|]3OwyixT#+_mgNc)g","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false}},"currentCostume":0,"costumes":[{"assetId":"a5053b5f2e662a7ca624012b2fed8253","name":"trees-a","bitmapResolution":1,"md5ext":"a5053b5f2e662a7ca624012b2fed8253.svg","dataFormat":"svg","rotationCenterX":49,"rotationCenterY":94},{"assetId":"10fd8cad9a5f03d4131fc51d1b091026","name":"trees-b","bitmapResolution":1,"md5ext":"10fd8cad9a5f03d4131fc51d1b091026.svg","dataFormat":"svg","rotationCenterX":36,"rotationCenterY":87}],"sounds":[{"assetId":"83a9787d4cb6f3b7632b4ddfebf74367","name":"pop","dataFormat":"wav","format":"","rate":44100,"sampleCount":1032,"md5ext":"83a9787d4cb6f3b7632b4ddfebf74367.wav"}],"volume":100,"visible":true,"x":117.82845188,"y":41.99441341,"size":100,"direction":90,"draggable":false,"rotationStyle":"all around"}],"meta":{"semver":"3.0.0","vm":"0.1.0-prerelease.1528399883","agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"}}
//"""
  val jsonstr = """
{"targets":[{"isStage":true,"name":"Stage","variables":{"`jEk@4|i[#Fk?(8x)AV.-my variable":["my variable",0],"Y})TGj3O`[./JoG:jwvt":["1234","0"]},"lists":{},"broadcasts":{},"blocks":{},"comments":{},"currentCostume":0,"costumes":[{"assetId":"cd21514d0531fdffb22204e0ec5ed84a","name":"backdrop1","md5ext":"cd21514d0531fdffb22204e0ec5ed84a.svg","dataFormat":"svg","rotationCenterX":1,"rotationCenterY":1}],"sounds":[{"assetId":"83a9787d4cb6f3b7632b4ddfebf74367","name":"pop","dataFormat":"wav","format":"","rate":44100,"sampleCount":1032,"md5ext":"83a9787d4cb6f3b7632b4ddfebf74367.wav"}],"volume":100,"tempo":60,"videoTransparency":50,"videoState":"off"},{"isStage":false,"name":"Sprite1","variables":{},"lists":{},"broadcasts":{},"blocks":{"@/O)M4y8_($x~d]L}$U6":{"opcode":"motion_movesteps","next":"M?{T{,6@P8:wyMb;`X*M","parent":null,"inputs":{"STEPS":[3,".1AU!D8HMhzdDRjLO6K;",[4,"10"]]},"fields":{},"topLevel":true,"shadow":false,"x":235,"y":116},"4qq%ah84}j5|}qj1ng+p":{"opcode":"operator_random","next":null,"parent":".1AU!D8HMhzdDRjLO6K;","inputs":{"FROM":[1,[4,"1"]],"TO":[3,"zG~vn1,k0m{jE:!Spg^Z",[4,"10"]]},"fields":{},"topLevel":false,"shadow":false},"M?{T{,6@P8:wyMb;`X*M":{"opcode":"motion_turnright","next":"CS=LrI+0EGR0`fvUp8:P","parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"DEGREES":[3,"K0F]oFCfOhT1xp{Q}]jE",[4,"15"]]},"fields":{},"topLevel":false,"shadow":false},"CS=LrI+0EGR0`fvUp8:P":{"opcode":"motion_turnleft","next":"SU%Y#S$q{]wh|L7fo{w=","parent":"M?{T{,6@P8:wyMb;`X*M","inputs":{"DEGREES":[1,[4,"15"]]},"fields":{},"topLevel":false,"shadow":false},"SU%Y#S$q{]wh|L7fo{w=":{"opcode":"data_setvariableto","next":"V[{TWX32fSm(wEjHkrHz","parent":"CS=LrI+0EGR0`fvUp8:P","inputs":{"VALUE":[1,[10,"0"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"e3*{*}+puG*N!QuUe1r|":{"opcode":"motion_turnright","next":"M|YrrN)uZ~Y9(pV#L.Ls","parent":"@,XEbej]//)~mN4DC%(F","inputs":{"DEGREES":[1,[4,"15"]]},"fields":{},"topLevel":false,"shadow":false},"M|YrrN)uZ~Y9(pV#L.Ls":{"opcode":"motion_turnleft","next":"E36AelTfbgJ@gm{tQCog","parent":"e3*{*}+puG*N!QuUe1r|","inputs":{"DEGREES":[1,[4,"15"]]},"fields":{},"topLevel":false,"shadow":false},"E36AelTfbgJ@gm{tQCog":{"opcode":"data_setvariableto","next":null,"parent":"M|YrrN)uZ~Y9(pV#L.Ls","inputs":{"VALUE":[1,[10,"0"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"@,XEbej]//)~mN4DC%(F":{"opcode":"data_setvariableto","next":"e3*{*}+puG*N!QuUe1r|","parent":null,"inputs":{"VALUE":[1,[10,"0"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":true,"shadow":false,"x":1301,"y":435},"V[{TWX32fSm(wEjHkrHz":{"opcode":"data_changevariableby","next":null,"parent":"SU%Y#S$q{]wh|L7fo{w=","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},".1AU!D8HMhzdDRjLO6K;":{"opcode":"operator_round","next":null,"parent":"@/O)M4y8_($x~d]L}$U6","inputs":{"NUM":[3,"4qq%ah84}j5|}qj1ng+p",[4,""]]},"fields":{},"topLevel":false,"shadow":false},"zG~vn1,k0m{jE:!Spg^Z":{"opcode":"operator_length","next":null,"parent":"4qq%ah84}j5|}qj1ng+p","inputs":{"STRING":[1,[10,"apple"]]},"fields":{},"topLevel":false,"shadow":false},"K0F]oFCfOhT1xp{Q}]jE":{"opcode":"operator_random","next":null,"parent":"M?{T{,6@P8:wyMb;`X*M","inputs":{"FROM":[1,[4,"1"]],"TO":[3,"jTVRWhz6O]-u;6cWl*Gv",[4,"10"]]},"fields":{},"topLevel":false,"shadow":false},"jTVRWhz6O]-u;6cWl*Gv":{"opcode":"operator_length","next":null,"parent":"K0F]oFCfOhT1xp{Q}]jE","inputs":{"STRING":[1,[10,"apple"]]},"fields":{},"topLevel":false,"shadow":false}},"comments":{},"currentCostume":0,"costumes":[{"assetId":"09dc888b0b7df19f70d81588ae73420e","name":"costume1","bitmapResolution":1,"md5ext":"09dc888b0b7df19f70d81588ae73420e.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55},{"assetId":"3696356a03a8d938318876a593572843","name":"costume2","bitmapResolution":1,"md5ext":"3696356a03a8d938318876a593572843.svg","dataFormat":"svg","rotationCenterX":47,"rotationCenterY":55}],"sounds":[{"assetId":"83c36d806dc92327b9e7049a565c6bff","name":"Meow","dataFormat":"wav","format":"","rate":44100,"sampleCount":37376,"md5ext":"83c36d806dc92327b9e7049a565c6bff.wav"}],"volume":100,"visible":true,"x":0,"y":0,"size":100,"direction":165,"draggable":false,"rotationStyle":"don't rotate"},{"isStage":false,"name":"Trees","variables":{},"lists":{},"broadcasts":{},"blocks":{"r/SAl@~c.KJhpmkZ`pDF":{"opcode":"data_setvariableto","next":"G1^19a55Lvjy#yrA5j[d","parent":null,"inputs":{"VALUE":[1,[10,"0"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":true,"shadow":false,"x":243,"y":96},"G1^19a55Lvjy#yrA5j[d":{"opcode":"data_changevariableby","next":"!2cvzsu;@JhqPwzThlO|","parent":"r/SAl@~c.KJhpmkZ`pDF","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"!2cvzsu;@JhqPwzThlO|":{"opcode":"data_changevariableby","next":"V+)c,1?u#R;{ojsq*j)M","parent":"G1^19a55Lvjy#yrA5j[d","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"V+)c,1?u#R;{ojsq*j)M":{"opcode":"data_changevariableby","next":"728wFTnx}oJf~%1!x=rx","parent":"!2cvzsu;@JhqPwzThlO|","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"728wFTnx}oJf~%1!x=rx":{"opcode":"data_changevariableby","next":"d_ZqRTo1|pWKa]XlvorA","parent":"V+)c,1?u#R;{ojsq*j)M","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"d_ZqRTo1|pWKa]XlvorA":{"opcode":"data_changevariableby","next":"mUn1UDqpsalM_-/;H)w/","parent":"728wFTnx}oJf~%1!x=rx","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"mUn1UDqpsalM_-/;H)w/":{"opcode":"data_changevariableby","next":"zSuuoa+|e4OZ.kG0AH,%","parent":"d_ZqRTo1|pWKa]XlvorA","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"zSuuoa+|e4OZ.kG0AH,%":{"opcode":"data_changevariableby","next":"6VGdPXpqZ8B[LFlH`+2?","parent":"mUn1UDqpsalM_-/;H)w/","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"6VGdPXpqZ8B[LFlH`+2?":{"opcode":"data_changevariableby","next":"_`xsixbi%[/x[yQpwGLp","parent":"zSuuoa+|e4OZ.kG0AH,%","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"k0|]3OwyixT#+_mgNc)g":{"opcode":"data_changevariableby","next":"4!aQqR|*RjV,E1`bMu)6","parent":"_`xsixbi%[/x[yQpwGLp","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"_`xsixbi%[/x[yQpwGLp":{"opcode":"data_changevariableby","next":"k0|]3OwyixT#+_mgNc)g","parent":"6VGdPXpqZ8B[LFlH`+2?","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false},"4!aQqR|*RjV,E1`bMu)6":{"opcode":"data_changevariableby","next":null,"parent":"k0|]3OwyixT#+_mgNc)g","inputs":{"VALUE":[1,[4,"1"]]},"fields":{"VARIABLE":["1234","Y})TGj3O`[./JoG:jwvt"]},"topLevel":false,"shadow":false}},"comments":{},"currentCostume":0,"costumes":[{"assetId":"a5053b5f2e662a7ca624012b2fed8253","name":"trees-a","bitmapResolution":1,"md5ext":"a5053b5f2e662a7ca624012b2fed8253.svg","dataFormat":"svg","rotationCenterX":49,"rotationCenterY":94},{"assetId":"10fd8cad9a5f03d4131fc51d1b091026","name":"trees-b","bitmapResolution":1,"md5ext":"10fd8cad9a5f03d4131fc51d1b091026.svg","dataFormat":"svg","rotationCenterX":36,"rotationCenterY":87}],"sounds":[{"assetId":"83a9787d4cb6f3b7632b4ddfebf74367","name":"pop","dataFormat":"wav","format":"","rate":44100,"sampleCount":1032,"md5ext":"83a9787d4cb6f3b7632b4ddfebf74367.wav"}],"volume":100,"visible":true,"x":117.82845188,"y":41.99441341,"size":100,"direction":90,"draggable":false,"rotationStyle":"all around"}],"meta":{"semver":"3.0.0","vm":"0.1.0-prerelease.1529940524","agent":"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.139 Safari/537.36"}}
"""
  val jsVal = Json.parse(jsonstr)
  val formattedJs = Json.prettyPrint(jsVal)
  println(formattedJs)

  val res = new Sb3Json2AST().programEl(jsVal)
  println(res)
  var resJson: JsObject = JsObject(Seq.empty)

  resJson = resJson + ("LongScript" -> new LongScriptAnalysis().analyze(res))
  resJson = resJson + ("UncommunicativeName" -> new UncommunicativeNameAnalysis().analyze(res))
  resJson = resJson + ("DuplicateCode" -> new DuplicateCodeAnalysis().analyze(jsonstr))
  resJson = resJson + ("DuplicateExpression" -> new DuplicateExpressionAnalysis().analyze(jsonstr))
//  resJson = resJson + ("BroadVariableScope" -> new BroadVariableScopeAnalysis().analyze(res))

  println(resJson.toString())

}