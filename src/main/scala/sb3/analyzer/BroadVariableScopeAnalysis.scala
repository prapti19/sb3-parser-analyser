package sb3.analyzer

import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsArray
import sb3.modifiedParser.{ ProgramEl, SpriteEl, ScriptEl, BlockEl, VariableEl }

class BroadVariableScopeAnalysis {
  def analyze(res: ProgramEl): JsArray = {
    var resArray: JsArray = JsArray(Seq.empty)

    res.sprites.foreach {
      eachSprite =>
        var varSet = scala.collection.mutable.Set[String]()
        eachSprite.scripts.foreach {
          eachScript =>
            var block = eachScript.topBlock
            while (block.next != null) {
              if (!block.fields.isEmpty) {
                block.fields.foreach {
                  eachField =>
                    if (eachField.fieldtype.equals("VARIABLE")) {
                      val variable = eachField.variable
                      if (!eachSprite.vars.contains(variable) && !varSet.contains(variable.id)) {
                        var smell: JsObject = JsObject(Seq.empty) + ("varBlock" -> JsString(eachField.variable.id)) 
                        smell = smell + ("name" -> JsString(eachField.variable.name))
                        resArray = resArray.append(smell)
                        varSet.add(variable.id)
                      }
                    }
                }
              }
              block = block.next
            }
        }
    }
    println("=========================================================================================")
    println(resArray.toString())
    resArray
  }
}