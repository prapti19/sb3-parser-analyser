package sb3.analyzer

import play.api.libs.json.JsObject
import play.api.libs.json.JsString
import play.api.libs.json.JsArray
import sb3.modifiedParser.{ ProgramEl, SpriteEl, ScriptEl, BlockEl, VariableEl }

class UncommunicativeNameAnalysis {
  def analyze(res: ProgramEl): JsArray = {
    var resArray: JsArray = JsArray(Seq.empty)

    //    if (isUncommunicative(res.stage.name)) {
    //      resJson = resJson + ("Stage" -> JsString(res.stage.name))
    //    }

    res.stage.vars.foreach {
      eachVar =>
        if (isUncommunicative(eachVar.name)) {
          var resJson: JsObject = JsObject(Seq.empty) + ("varBlock" -> JsString(eachVar.id)) + ("name" -> JsString(eachVar.name))
          resArray = resArray.append(resJson)
        }
    }

    res.sprites.foreach {
      eachSprite =>
        eachSprite.vars.foreach {
          eachVar =>
            if (isUncommunicative(eachVar.name)) {
              var resJson: JsObject = JsObject(Seq.empty) + ("varBlock" -> JsString(eachVar.id)) + ("name" -> JsString(eachVar.name))
              resArray = resArray.append(resJson)
            }
        }
    }
    println("=========================================================================================")
    println(resArray.toString())
    resArray
  }

  def isUncommunicative(name: String): Boolean = {
    val Pattern = "[a-zA-Z]+[0-9a-zA-Z]*[ _]?[a-zA-Z]*".r
    val SpritePattern = "[Ss]prite[0-9]*".r
    name match {
      case Pattern() => name match {
        case SpritePattern() => true
        case _ => false
      }
      case _ => true

    }
  }
}
