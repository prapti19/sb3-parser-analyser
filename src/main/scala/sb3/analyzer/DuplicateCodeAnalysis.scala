package sb3.analyzer

import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import play.api.libs.json.JsObject
import play.api.libs.json.Json

import sb3.parser._
import scala.collection.JavaConversions._
import sb3.parser.ProgramEl
import ast.Sprite
import ast.Script
import ast.Program
import ast.Stage
import ast.ExprStmt
import sb3.parser.json.Sb3Json2Block
import clone.analyzer.CloneStmtSeqAnalyzer
import clone.result.CloneSeqResult

class DuplicateCodeAnalysis {
  def analyze(jsonstr: String): JsArray = {
    var resArray: JsArray = JsArray(Seq.empty)

    val jsVal = Json.parse(jsonstr)
    val formattedJs = Json.prettyPrint(jsVal)

    val res = new Sb3Json2Block().programEl(jsVal)

    res.stage.sprites.foreach {
      eachSprite =>
        val sprite = eachSprite.toAst()
        val program = programOf(sprite)
        val analyzer = new CloneStmtSeqAnalyzer()
        val res = analyzer.analyze(program)
        val result = CloneSeqResult.of(res)
        val groups = result.groups()
        var eachGroup: JsArray = JsArray(Seq.empty)
        groups.toList.foreach {
          group =>
            var resGroup: JsArray = JsArray(Seq.empty)
            val g = group.getGroup()
            g.toList.foreach {
              stack =>
                val stmts = stack.get.toList
                val exprstmts = stmts.map(stmt => stmt.asInstanceOf[ExprStmt])
                var resJson: JsObject = JsObject(Seq.empty) + ("topBlock" -> JsString(exprstmts.head.getBlockId))
                resJson = resJson + ("bottomBlock" -> JsString(exprstmts.last.getBlockId))
                resGroup = resGroup.append(resJson)
            }
            if(resGroup.value.size>1) eachGroup = eachGroup.append(JsObject(Seq.empty) + ("noOfGroups" -> JsString(resGroup.value.size.toString())) + ("groups" -> resGroup))
        }
        resArray = resArray.append(eachGroup)
    }
    println("=========================================================================================")
    println(resArray.toString())
    resArray
  }
  
  def programOf(sprite: Sprite): Program = {
    val program = new Program();
		program.setStage(new Stage());
		program.getStage().addSprite(sprite);
		return program;
  }
}
