package sb3.analyzer

import play.api.libs.json.JsArray
import play.api.libs.json.JsString
import play.api.libs.json.JsObject
import sb3.modifiedParser.{ ProgramEl, SpriteEl, ScriptEl, BlockEl, VariableEl }

class LongScriptAnalysis {
  def analyze(res: ProgramEl): JsArray = {
    var resArray: JsArray = JsArray(Seq.empty)

    res.sprites.foreach {
      eachSprite =>
        var smells: JsArray = JsArray(Seq.empty)
        eachSprite.scripts.foreach {
          eachScript =>
            var length = 1
            var block = eachScript.topBlock
            while (block.next != null) {
              block = block.next
              length = length + 1
            }
            if (length > 11) {
              var smell: JsObject = JsObject(Seq.empty) + ("topBlock" -> JsString(eachScript.topBlockID)) + ("bottomBlock" -> JsString(block.id)) + ("length" -> JsString(length.toString()))
              smells = smells.append(smell)
            }
        }
        resArray = resArray.append(smells)
    }
    println("=========================================================================================")
    println(resArray.toString())
    resArray
  }
}